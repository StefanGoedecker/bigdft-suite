module pesbias

    use dynamic_memory
        private
        integer,parameter :: ns=1
        integer,parameter :: np=0
        integer, save :: nat
        real(8), save :: ampl,gaussian_width
        real(8), dimension(:), pointer :: eval_ref=>null() !(nat*(ns+3*np))
        real(8), dimension(:), pointer :: rcov=>null() !(nat)

        public :: initEVALpenalty,addEVALpenalty,finaliseEvalpenalty,set_reference_fingerprint

contains

subroutine set_reference_rcov(nat,rcov_bias)
    real(8), dimension(nat), intent(in) :: rcov_bias
    rcov=f_malloc_ptr(nat,id='rcov')
    call f_memcpy(src=rcov_bias,dest=rcov)
end subroutine

    subroutine set_reference_fingerprint(rcov_bias,filename)   ! called from global
    use module_atoms
    use module_base, only: bigdft_mpi
    implicit real*8 (a-h,o-z)

    character(len=*), intent(in) :: filename
    real*8,allocatable ::  devaldr(:,:,:)
    real(8), dimension(:), intent(in) :: rcov_bias
    !local variables
    type(atomic_structure) :: astruct

    call nullify_atomic_structure(astruct)

    call set_astruct_from_file(filename,bigdft_mpi%iproc,astruct)

    nat=astruct%nat

    call set_reference_rcov(nat,rcov_bias)
    allocate(devaldr(3,nat,nat*(ns+np*3)))

    !write(*,*) 'ampl,gaussian_width',ampl,gaussian_width
    !  do i=1,nat
    ! write(*,'(i3,3(1x,e10.3),4x,e10.3)') i,(rxyz(l,i),l=1,3),rcov(i)
    !  enddo
   eval_ref=f_malloc_ptr(nat*(ns+3*np),id='eval_ref')

     call xyz2devaldr(astruct%nat,astruct%rxyz,ns,np,rcov,eval_ref,.false.,devaldr)

    deallocate(devaldr)

    call deallocate_atomic_structure(astruct)

        end subroutine


subroutine initEVALpenalty(natp,ampl_bias,gw_bias)   ! Called from bigdft_state
!  calculates the penalty (energy pllus fingerprint distance and their derivative penalder(*,*)
  implicit real*8 (a-h,o-z)

     nat=natp
     ampl=ampl_bias
     gaussian_width=gw_bias
     !rcov_bias=f_malloc(nat,id='rcov_bias')


     !open(file='EVALref',unit=765)
!
!     read(765,*) ampl
!     read(765,*) gaussian_width
!     do i=1,nat
!     read(765,*) rcov_bias(i)
!     enddo
!     do i=1,nat*(ns+3*np)
!     read(765,*) eval_ref(i)
!     enddo
 end subroutine initEVALpenalty

subroutine finaliseEVALpenalty()
    call f_free_ptr(eval_ref)
    call f_free_ptr(rcov)
end subroutine

subroutine addEVALpenalty(rxyz,etot,fxyz)
!  calculates the penalty (energy pllus fingerprint distance and their derivative penalder(*,*)
  implicit real*8 (a-h,o-z)
  dimension rxyz(3,nat),penalder(3,nat),eval(nat*(ns+3*np)),fxyz(3,nat)
  real*8,allocatable ::  devaldr(:,:,:)

  allocate(devaldr(3,nat,nat*(ns+np*3)))
     ampl=1.d0
     gaussian_width=.1d0

    !write(*,*) 'ampl,gaussian_width',ampl,gaussian_width
    !  do i=1,nat
    ! write(*,'(i3,3(1x,e10.3),4x,e10.3)') i,(rxyz(l,i),l=1,3),rcov_bias(i)
    !  enddo

     call xyz2devaldr(nat,rxyz,ns,np,rcov,eval,.false.,devaldr)

     penal=0.d0
     do l=1,nat*(ns+3*np)
     penal=penal+(eval(l)-eval_ref(l))**2
      !write(*,*) i,eval(l),eval_ref(l)
     enddo
     !epsilon=1.d-1
     !penal=sqrt(penal+ epsilon)  ! Add this epsilon to have well defined derivatives (prevent division by zero)

     !write(*,*) 'initial force norm',dnrm2(3*nat,fxyz,1)
     do l=1,nat*(ns+3*np)
        fact=2.d0*ampl*(eval(l)-eval_ref(l))
        !fact=ampl*(eval(l)-eval_ref(l))/penal
     do kat=1,nat
         fxyz(1,kat)=fxyz(1,kat) - fact*devaldr(1,kat,l)
         fxyz(2,kat)=fxyz(2,kat) - fact*devaldr(2,kat,l)
         fxyz(3,kat)=fxyz(3,kat) - fact*devaldr(3,kat,l)
     enddo
     enddo

     !write(*,*) 'final force norm',dnrm2(3*nat,fxyz,1)
     write(864,'(a,3(2x,e12.5))') 'etot,penal,ampl*penal=',etot,penal,ampl*penal
     etot=etot+ampl*penal

 ! check whether all forces sum up to zero; can be commented out
    sumx = 0.d0
    sumy = 0.d0
    sumz = 0.d0
    do jat = 1,nat
      sumx = sumx + fxyz(1,jat)
      sumy = sumy + fxyz(2,jat)
      sumz = sumz + fxyz(3,jat)
    enddo
    if(sumx**2 .gt. 1.d-12) write(*,*) 'sumx',sumx
    if(sumy**2 .gt. 1.d-12) write(*,*) 'sumy',sumy
    if(sumz**2 .gt. 1.d-12) write(*,*) 'sumz',sumz

  deallocate(devaldr)

end subroutine addEVALpenalty


end module pesbias



subroutine xyz2devaldr(nat,rxyz,ns,np,rcov,eval,eval_only,devaldr)
! Calculqates the derivative of all eigenvalues of an atomic fingerprint with respect to the atomic positions
  implicit real*8 (a-h,o-z)
  logical eval_only
  dimension rxyz(3,nat), rcov(nat),eval(nat*(ns+np*3)),devaldr(3,nat,nat*(ns+np*3))
  dimension evalsmooth(nat*(ns+np*3)),devalsmoothdr(3,nat,nat*(ns+np*3)),grad(3,nat)
  real*8,allocatable ::  ovrlp(:,:), evec(:,:),work(:)
  real*8  alpha(nat), cs(10),cp(10)

    do iat=1,nat
     alpha(iat)=.5d0/rcov(iat)**2
    enddo
    ! Specify the width of the Gaussians if several Gaussians per l-channel are used
    do i=1,10
      cs(i)=sqrt(2.d0)**(i-1)
      cp(i)=sqrt(2.d0)**(i-1)
    enddo

    nt=3*np+ns
    norb= nat*(ns+np*3)
    allocate(ovrlp(norb,norb),evec(norb,norb))
    lwork=100*norb
    allocate(work(lwork))


    do iat=1,nat
       alpha(iat)=.5d0/rcov(iat)**2
    enddo

    call crtovrlp(nat,rxyz,alpha,cs,cp,ns,np,ovrlp)

      do jorb=1,norb
      do iorb=1,norb
      evec(iorb,jorb)=ovrlp(iorb,jorb)
      enddo
      enddo
     call dsyev('V','L', norb, evec, norb, eval, work, lwork, info)
     if(info/=0) stop ' ERROR in dsyev'
  ! eigenvalues in decreasing order
     do i=1,norb/2
        t1=eval(i)
        t2=eval(norb-i+1)
        eval(i)=t2
        eval(norb-i+1)=t1
     enddo
     if (eval_only) goto 1122

! Now calculate derivatives
    do iat=1,nat
        do iorb=1,norb
          devaldr(1,iat,iorb)=0.d0
          devaldr(2,iat,iorb)=0.d0
          devaldr(3,iat,iorb)=0.d0
        enddo
    enddo

  !  <s|s>
  do jat=1,nat
    do js=1,ns
      jorb=(jat-1)*nt+js
      aj=alpha(jat)/cs(js)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat= 1,nat
        do is=1,ns
          !!iorb=iat+(is-1)*nat
          iorb=(iat-1)*nt+is
          ai= alpha(iat)/cs(is) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 
          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=ovrlp(iorb,jorb)

          ! derivatives
          tt= -4.d0*t1/t2

          do korb=1,norb
            kkorb=norb-korb+1  ! Also deriavtive in decreasing order of eigenvalues

            deri = tt*sij*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)
       
            deri = tt*sij*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = tt*sij*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)      
          enddo
        enddo
      enddo
    enddo  
  enddo  

  !  <pi|sj>
  do jat=1,nat ! kat, kat ! 1,nat
    do js=1,ns
      
      jorb=(jat-1)*nt+js
      aj=alpha(jat)/cs(js)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do ip=1,np

          iorb=(iat-1)*nt+ns+ip
          ai= alpha(iat)/cp(ip) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 

          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t3=-4.d0*sqrt(ai)*aj/t2

          ! derivatives
          t5=-4.d0*t1/t2 

          do korb=1,norb
           
            kkorb=norb-korb+1  ! Alsoe deriavtive in decreainsg order of eigenvalues
            deri = ovrlp(iorb,jorb)*t5*xij+t3*sij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb,jorb)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb,jorb)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+1,jorb)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+1,jorb)*t5*yij+t3*sij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+1,jorb)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+2,jorb)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+2,jorb)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb+2,jorb)*t5*zij+t3*sij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)
          enddo        
        enddo
      enddo
    enddo  
  enddo  


  !  <si|pj> 
  do jat=1,nat
    do jp=1,np
          
      jorb=(jat-1)*nt+ns+jp
      aj=alpha(jat)/cp(jp)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do is=1,ns
          !!iorb=iat+(is-1)*nat
          iorb=(iat-1)*nt+is
          ai= alpha(iat)/cs(is) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 

          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t3=+4.d0*sqrt(aj)*ai/t2

          ! derivatives
          !tt= -2.d0*t1/t2 * sij
          t5=-4.d0*t1/t2 
         
          do korb=1,norb
            kkorb=norb-korb+1  ! Alsoe deriavtive in decreainsg order of eigenvalues

            deri = ovrlp(iorb,jorb)*t5*xij+t3*sij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb,jorb)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb,jorb)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

            deri = ovrlp(iorb,jorb+1)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)

            deri = ovrlp(iorb,jorb+1)*t5*yij+t3*sij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)

            deri = ovrlp(iorb,jorb+1)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)

            deri = ovrlp(iorb,jorb+2)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)

            deri = ovrlp(iorb,jorb+2)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)

            deri = ovrlp(iorb,jorb+2)*t5*zij+t3*sij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)
          enddo
    
        enddo
      enddo
    enddo  
  enddo  


  !  <p|p>
  do jat=1,nat
    do jp=1,np
        
      jorb=(jat-1)*nt+ns+jp
      aj=alpha(jat)/cp(jp)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do ip=1,np
          !!iorb=1+(iat-1)*3 +ns*nat+(ip-1)*3*nat
          iorb=(iat-1)*nt+ns+ip
          ai= alpha(iat)/cp(ip) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 
          t1=ai*aj
          t2=ai+aj

          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t4= 2.d0*sqrt(t1)/t2 
          t5=-4.d0*t1/t2 

          ! derivatives
      
          do korb=1,norb
            kkorb=norb-korb+1  ! Alsoe deriavtive in decreainsg order of eigenvalues
             
            !domdr(jorb  ,iorb  ,1) = ovrlp(iorb  ,jorb  )* dij*t5*xij + t4*t5*sij*dij* xij *2.d0
            deri = ovrlp(iorb,jorb)*t5*xij+t4*t5*sij*xij*2.d0
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

             !domdr(jorb  ,iorb  ,2) = ovrlp(iorb  ,jorb  )* dij*t5*yij
            deri = ovrlp(iorb,jorb)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)

               !domdr(jorb  ,iorb  ,3) = ovrlp(iorb  ,jorb  )* dij*t5*zij
            deri = ovrlp(iorb,jorb)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb,korb)
                                  
             !domdr(jorb+1,iorb  ,1) = ovrlp(iorb+1,jorb  )* dij*t5*xij + t4*t5*sij*dij* yij  
            deri = ovrlp(iorb+1,jorb)*t5*xij+t4*t5*sij*yij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)

             !domdr(jorb+1,iorb  ,2) = ovrlp(iorb+1,jorb  )* dij*t5*yij + t4*t5*sij*dij* xij 
            deri = ovrlp(iorb+1,jorb)*t5*yij+t4*t5*sij*xij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)

             !!domdr(jorb+1,iorb  ,3) = ovrlp(iorb+1,jorb  )* dij*t5*zij
            deri = ovrlp(iorb+1,jorb)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb,korb)
                                  
             !domdr(jorb+2,iorb  ,1) = ovrlp(iorb+2,jorb  )* dij*t5*xij + t4*t5*sij*dij* zij 
            deri = ovrlp(iorb+2,jorb)*t5*xij+t4*t5*sij*zij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)

             !domdr(jorb+2,iorb  ,2) = ovrlp(iorb+2,jorb  )* dij*t5*yij 
            deri = ovrlp(iorb+2,jorb)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)

             !domdr(jorb+2,iorb  ,3) = ovrlp(iorb+2,jorb  )* dij*t5*zij + t4*t5*sij*dij* xij 
            deri = ovrlp(iorb+2,jorb)*t5*zij+t4*t5*sij*xij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb,korb)

             !domdr(jorb  ,iorb+1,1) = ovrlp(iorb  ,jorb+1)* dij*t5*xij + t4*t5*sij*dij* yij 
            deri = ovrlp(iorb,jorb+1)*t5*xij+t4*t5*sij*yij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb  ,iorb+1,2) = ovrlp(iorb  ,jorb+1)* dij*t5*yij + t4*t5*sij*dij* xij 
            deri = ovrlp(iorb,jorb+1)*t5*yij+t4*t5*sij*xij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb  ,iorb+1,3) = ovrlp(iorb  ,jorb+1)* dij*t5*zij
            deri = ovrlp(iorb,jorb+1)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+1,korb)
                                  
             !domdr(jorb+1,iorb+1,1) = ovrlp(iorb+1,jorb+1)* dij*t5*xij 
            deri = ovrlp(iorb+1,jorb+1)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb+1,iorb+1,2) = ovrlp(iorb+1,jorb+1)* dij*t5*yij + t4*t5*sij*dij* yij*2.d0
            deri = ovrlp(iorb+1,jorb+1)*t5*yij+t4*t5*sij*yij*2.d0
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb+1,iorb+1,3) = ovrlp(iorb+1,jorb+1)* dij*t5*zij
            deri = ovrlp(iorb+1,jorb+1)*t5*zij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb+2,iorb+1,1) = ovrlp(iorb+2,jorb+1)* dij*t5*xij 
            deri = ovrlp(iorb+2,jorb+1)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb+2,iorb+1,2) = ovrlp(iorb+2,jorb+1)* dij*t5*yij + t4*t5*sij*dij* zij
            deri = ovrlp(iorb+2,jorb+1)*t5*yij+t4*t5*sij*zij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb+2,iorb+1,3) = ovrlp(iorb+2,jorb+1)* dij*t5*zij + t4*t5*sij*dij* yij 
            deri = ovrlp(iorb+2,jorb+1)*t5*zij+t4*t5*sij*yij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+1,korb)

             !domdr(jorb  ,iorb+2,1) = ovrlp(iorb  ,jorb+2)* dij*t5*xij + t4*t5*sij*dij* zij 
            deri = ovrlp(iorb,jorb+2)*t5*xij+t4*t5*sij*zij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb  ,iorb+2,2) = ovrlp(iorb  ,jorb+2)* dij*t5*yij
            deri = ovrlp(iorb,jorb+2)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb  ,iorb+2,3) = ovrlp(iorb  ,jorb+2)* dij*t5*zij + t4*t5*sij*dij* xij 
            deri = ovrlp(iorb,jorb+2)*t5*zij+t4*t5*sij*xij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb+1,iorb+2,1) = ovrlp(iorb+1,jorb+2)* dij*t5*xij 

            deri = ovrlp(iorb+1,jorb+2)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb+1,iorb+2,2) = ovrlp(iorb+1,jorb+2)* dij*t5*yij + t4*t5*sij*dij* zij 
            deri = ovrlp(iorb+1,jorb+2)*t5*yij+t4*t5*sij*zij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb+1,iorb+2,3) = ovrlp(iorb+1,jorb+2)* dij*t5*zij + t4*t5*sij*dij* yij 
            deri = ovrlp(iorb+1,jorb+2)*t5*zij+t4*t5*sij*yij
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+1,korb)*deri*evec(jorb+2,korb)
                                 
             !domdr(jorb+2,iorb+2,1) = ovrlp(iorb+2,jorb+2)* dij*t5*xij
            deri = ovrlp(iorb+2,jorb+2)*t5*xij
            devaldr(1,iat,kkorb)=devaldr(1,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb+2,iorb+2,2) = ovrlp(iorb+2,jorb+2)* dij*t5*yij 
            deri = ovrlp(iorb+2,jorb+2)*t5*yij
            devaldr(2,iat,kkorb)=devaldr(2,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+2,korb)

             !domdr(jorb+2,iorb+2,3) = ovrlp(iorb+2,jorb+2)* dij*t5*zij + t4*t5*sij*dij* zij *2.d0 
            deri = ovrlp(iorb+2,jorb+2)*t5*zij+t4*t5*sij*zij*2.d0
            devaldr(3,iat,kkorb)=devaldr(3,iat,kkorb)+evec(iorb+2,korb)*deri*evec(jorb+2,korb)
          enddo  
        enddo
      enddo
    enddo  
  enddo  

  deallocate(evec)
  deallocate(ovrlp)
  deallocate(work)

  
1122 continue

if (gaussian_width.ne.0.d0) then
      !write(*,*) 'smoothing done'
     a2inv=1.d0/gaussian_width**2
     length=nat*(ns+np*3)


  if (eval_only) then 
     do i=1,length
             evalsmooth(i)=0.d0  !new fp
              tt=0.d0
              do l=1,length
                pil=exp((-0.5d0*a2inv)*(eval(i)-eval(l))**2)
                evalsmooth(i)=evalsmooth(i)+eval(l)*pil
                tt=tt+pil
              enddo
              ttinv=1.d0/tt
              evalsmooth(i)=evalsmooth(i)*ttinv
     enddo
! overwrite original quantities by smoothed quantities
     do i=1,length
        eval(i)=evalsmooth(i)
     enddo

  else

     do i=1,length
             evalsmooth(i)=0.d0  !new fp
             do iat=1,nat
             do j=1,3
             devalsmoothdr(j,iat,i)=0.d0
             grad(j,iat)=0.d0
             enddo
             enddo
              tt=0.d0
              do l=1,length
                pil=exp((-0.5d0*a2inv)*(eval(i)-eval(l))**2)
                evalsmooth(i)=evalsmooth(i)+eval(l)*pil

                dil=a2inv*(eval(l)-eval(i))*pil
                do iat=1,nat
                do j=1,3
                devalsmoothdr(j,iat,i)=devalsmoothdr(j,iat,i)+pil*devaldr(j,iat,l)   & 
                    +eval(l)*(dil*(devaldr(j,iat,i)-devaldr(j,iat,l)))
                grad(j,iat)=grad(j,iat)+(dil*(devaldr(j,iat,i)-devaldr(j,iat,l)))
                enddo
                enddo
                tt=tt+pil
  
              enddo
              ttinv=1.d0/tt
              evalsmooth(i)=evalsmooth(i)*ttinv
                do iat=1,nat
                do j=1,3
                devalsmoothdr(j,iat,i)=ttinv*(devalsmoothdr(j,iat,i)-evalsmooth(i)*grad(j,iat))
                enddo
                enddo

     enddo
! overwrite original quantities by smoothed quantities
     do i=1,length
        eval(i)=evalsmooth(i)
        do  iat=1,nat
        do  j=1,3
           devaldr(j,iat,i)=devalsmoothdr(j,iat,i)
        enddo
        enddo
     enddo

  endif
endif

end subroutine xyz2devaldr



subroutine crtovrlp(nat,rxyz,alpha,cs,cp,ns,np,ovrlp)
  implicit real*8 (a-h,o-z)
  real*8  rxyz(3,nat)
  real*8 ovrlp(nat*(ns+3*np),nat*(ns+3*np))
  real*8  alpha(nat), cs(10),cp(10)


  if(ns>10 .or. np > 10) stop 'ns > 10   .or.  np > 10  !'


 ! 1- setup the overlap matrix 

  !  <s|s>
  do jat=1,nat
    do js=1,ns
      jorb=(jat-1)*(ns+3*np)+js
      aj=alpha(jat)/cs(js)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do is=1,ns
          !!iorb=iat+(is-1)*nat
          iorb=(iat-1)*(ns+3*np)+is
          ai= alpha(iat)/cs(is) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 
          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          ovrlp(iorb,jorb)=sij

        enddo
      enddo
    enddo  
  enddo  


  !  <pi|sj>
  do jat=1,nat
    do js=1,ns
      
      jorb=(jat-1)*(ns+3*np)+js
      aj=alpha(jat)/cs(js)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do ip=1,np
          !!iorb=1+(iat-1)*3+ns*nat + (ip-1)*3*nat
          iorb=(iat-1)*(ns+3*np)+ns+ip
          ai= alpha(iat)/cp(ip) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 

          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t3=-2.d0*sqrt(ai)*aj/t2
          ovrlp(iorb  ,jorb  )= t3 * xij *sij
          ovrlp(iorb+1,jorb  )= t3 * yij *sij
          ovrlp(iorb+2,jorb  )= t3 * zij *sij

        enddo
      enddo
    enddo  
  enddo  


  !  <si|pj> 
  do jat=1,nat
    do jp=1,np
        
      jorb=(jat-1)*(ns+3*np)+ns+jp
      aj=alpha(jat)/cp(jp)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do is=1,ns
          !!iorb=iat+(is-1)*nat
          iorb=(iat-1)*(ns+3*np)+is
          ai= alpha(iat)/cs(is) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 

          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t3=+2.d0*sqrt(aj)*ai/t2
          ovrlp(iorb,jorb  )= t3 * xij *sij
          ovrlp(iorb,jorb+1)= t3 * yij *sij
          ovrlp(iorb,jorb+2)= t3 * zij *sij

        enddo
      enddo
    enddo  
  enddo  


  !  <p|p>
  do jat=1,nat
    do jp=1,np
      
      jorb=(jat-1)*(ns+3*np)+ns+jp
      aj=alpha(jat)/cp(jp)
      xj=rxyz(1,jat) ; yj=rxyz(2,jat); zj=rxyz(3,jat)

      do iat=1,nat
        do ip=1,np
          iorb=(iat-1)*(ns+3*np)+ns+ip
          ai= alpha(iat)/cp(ip) 
          xi=rxyz(1,iat) ; yi=rxyz(2,iat); zi=rxyz(3,iat)

          xij=xi-xj; yij=yi-yj; zij=zi-zj
          r2=xij**2 + yij**2 + zij**2 
          t1=ai*aj
          t2=ai+aj

          ! normalized GTOs:
          sij=sqrt(2.d0*sqrt(t1)/t2)**3 * exp (-t1/t2*r2) 
          t4= 2.d0*sqrt(t1)/t2 
          t5=-2.d0*t1/t2 

          ovrlp(iorb  ,jorb  )= t4 *(1.d0 + t5* xij* xij)  * sij
          ovrlp(iorb+1,jorb  )= t4 *(       t5* yij* xij)  * sij
          ovrlp(iorb+2,jorb  )= t4 *(       t5* zij* xij)  * sij
          ovrlp(iorb  ,jorb+1)= t4 *(       t5* xij* yij)  * sij
          ovrlp(iorb+1,jorb+1)= t4 *(1.d0+  t5* yij* yij)  * sij
          ovrlp(iorb+2,jorb+1)= t4 *(       t5* zij* yij)  * sij
          ovrlp(iorb  ,jorb+2)= t4 *(       t5* xij* zij)  * sij
          ovrlp(iorb+1,jorb+2)= t4 *(       t5* yij* zij)  * sij
          ovrlp(iorb+2,jorb+2)= t4 *(1.d0+  t5* zij* zij)  * sij

        enddo
      enddo
    enddo  
  enddo  

end subroutine crtovrlp


